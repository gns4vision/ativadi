(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["/system/angular/main"],{

/***/ "./resources/assets/system/angular/auditorias/auditoria-controller.js":
/*!****************************************************************************!*\
  !*** ./resources/assets/system/angular/auditorias/auditoria-controller.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ['$scope', function ($scope) {
  $scope.auditorias = _auditorias;
}];

/***/ }),

/***/ "./resources/assets/system/angular/lengthLetters.js":
/*!**********************************************************!*\
  !*** ./resources/assets/system/angular/lengthLetters.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports = [function () {
  var LetterSize =
  /*#__PURE__*/
  function () {
    function LetterSize() {
      _classCallCheck(this, LetterSize);
    }

    _createClass(LetterSize, [{
      key: "increaseSize",
      value: function increaseSize() {
        var p = document.getElementsByTagName("p");
        var h1 = document.getElementsByTagName("h1");
        var h2 = document.getElementsByTagName("h2");
        var h3 = document.getElementsByTagName("h3");
        var h4 = document.getElementsByTagName("h4");
        var h5 = document.getElementsByTagName("h5");
        var h6 = document.getElementsByTagName("h6");
        var strong = document.getElementsByTagName("strong");
        var span = document.getElementsByTagName("span");
        var th = document.getElementsByTagName("th");
        var td = document.getElementsByTagName("td");
        var i = document.getElementsByTagName("i");
        var a = document.getElementsByTagName("a");

        for (var incre = 0; incre < p.length; incre++) {
          p[incre].style.fontSize = "large";
        }

        for (var _incre = 0; _incre < h1.length; _incre++) {
          h1[_incre].style.fontSize = "large";
        }

        for (var _incre2 = 0; _incre2 < h2.length; _incre2++) {
          h2[_incre2].style.fontSize = "large";
        }

        for (var _incre3 = 0; _incre3 < h3.length; _incre3++) {
          h3[_incre3].style.fontSize = "large";
        }

        for (var _incre4 = 0; _incre4 < h4.length; _incre4++) {
          h4[_incre4].style.fontSize = "large";
        }

        for (var _incre5 = 0; _incre5 < h5.length; _incre5++) {
          h5[_incre5].style.fontSize = "large";
        }

        for (var _incre6 = 0; _incre6 < h6.length; _incre6++) {
          h6[_incre6].style.fontSize = "large";
        }

        for (var _incre7 = 0; _incre7 < strong.length; _incre7++) {
          strong[_incre7].style.fontSize = "large";
        }

        for (var _incre8 = 0; _incre8 < span.length; _incre8++) {
          span[_incre8].style.fontSize = "large";
        }

        for (var _incre9 = 0; _incre9 < th.length; _incre9++) {
          th[_incre9].style.fontSize = "large";
        }

        for (var _incre10 = 0; _incre10 < td.length; _incre10++) {
          td[_incre10].style.fontSize = "large";
        }

        for (var _incre11 = 0; _incre11 < i.length; _incre11++) {
          i[_incre11].style.fontSize = "large";
        }

        for (var _incre12 = 0; _incre12 < a.length; _incre12++) {
          a[_incre12].style.fontSize = "large";
        }
      }
    }, {
      key: "decreaseSize",
      value: function decreaseSize() {
        var p = document.getElementsByTagName("p");
        var h1 = document.getElementsByTagName("h1");
        var h2 = document.getElementsByTagName("h2");
        var h3 = document.getElementsByTagName("h3");
        var h4 = document.getElementsByTagName("h4");
        var h5 = document.getElementsByTagName("h5");
        var h6 = document.getElementsByTagName("h6");
        var strong = document.getElementsByTagName("strong");
        var span = document.getElementsByTagName("span");
        var th = document.getElementsByTagName("th");
        var td = document.getElementsByTagName("td");
        var i = document.getElementsByTagName("i");
        var a = document.getElementsByTagName("a");

        for (var incre = 0; incre < p.length; incre++) {
          p[incre].style.fontSize = "initial";
        }

        for (var _incre13 = 0; _incre13 < h1.length; _incre13++) {
          h1[_incre13].style.fontSize = "initial";
        }

        for (var _incre14 = 0; _incre14 < h2.length; _incre14++) {
          h2[_incre14].style.fontSize = "initial";
        }

        for (var _incre15 = 0; _incre15 < h3.length; _incre15++) {
          h3[_incre15].style.fontSize = "initial";
        }

        for (var _incre16 = 0; _incre16 < h4.length; _incre16++) {
          h4[_incre16].style.fontSize = "initial";
        }

        for (var _incre17 = 0; _incre17 < h5.length; _incre17++) {
          h5[_incre17].style.fontSize = "initial";
        }

        for (var _incre18 = 0; _incre18 < h6.length; _incre18++) {
          h6[_incre18].style.fontSize = "initial";
        }

        for (var _incre19 = 0; _incre19 < strong.length; _incre19++) {
          strong[_incre19].style.fontSize = "initial";
        }

        for (var _incre20 = 0; _incre20 < span.length; _incre20++) {
          span[_incre20].style.fontSize = "initial";
        }

        for (var _incre21 = 0; _incre21 < th.length; _incre21++) {
          th[_incre21].style.fontSize = "initial";
        }

        for (var _incre22 = 0; _incre22 < td.length; _incre22++) {
          td[_incre22].style.fontSize = "initial";
        }

        for (var _incre23 = 0; _incre23 < i.length; _incre23++) {
          i[_incre23].style.fontSize = "initial";
        }

        for (var _incre24 = 0; _incre24 < a.length; _incre24++) {
          a[_incre24].style.fontSize = "initial";
        }
      }
    }], [{
      key: "getInstance",
      value: function getInstance() {
        if (this.instance == null || this.instance == undefined) {
          this.instance = new LetterSize();
        }

        return this.instance;
      }
    }]);

    return LetterSize;
  }();

  return LetterSize;
}];

/***/ }),

/***/ "./resources/assets/system/angular/main.js":
/*!*************************************************!*\
  !*** ./resources/assets/system/angular/main.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var app = angular.module('app', ['ui.utils.masks']).controller('SystemController', __webpack_require__(/*! ./system-controller */ "./resources/assets/system/angular/system-controller.js")).controller('TurmaController', __webpack_require__(/*! ./turmas/turma-controller */ "./resources/assets/system/angular/turmas/turma-controller.js")).controller('TurmaCreateController', __webpack_require__(/*! ./turmas/turma-create-controller */ "./resources/assets/system/angular/turmas/turma-create-controller.js")).controller('AuditoriaController', __webpack_require__(/*! ./auditorias/auditoria-controller */ "./resources/assets/system/angular/auditorias/auditoria-controller.js")).controller('AtividadeController', __webpack_require__(/*! ./atividade/atividade-controller */ "./resources/assets/system/angular/atividade/atividade-controller.js")).controller('AtividadeCreateController', __webpack_require__(/*! ./atividade/atividade-create-controller */ "./resources/assets/system/angular/atividade/atividade-create-controller.js")).service('LetterSize', __webpack_require__(/*! ./lengthLetters */ "./resources/assets/system/angular/lengthLetters.js")).service('TurmaService', __webpack_require__(/*! ./turmas/turma-service */ "./resources/assets/system/angular/turmas/turma-service.js")).service('AtividadeService', __webpack_require__(/*! ./atividade/atividade-service */ "./resources/assets/system/angular/atividade/atividade-service.js")).service('ToastrService', __webpack_require__(/*! ./observer/toastrService */ "./resources/assets/system/angular/observer/toastrService.js"));

/***/ }),

/***/ "./resources/assets/system/angular/observer/toastrService.js":
/*!*******************************************************************!*\
  !*** ./resources/assets/system/angular/observer/toastrService.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports = [function () {
  var ToastrService =
  /*#__PURE__*/
  function () {
    function ToastrService() {
      _classCallCheck(this, ToastrService);
    }

    _createClass(ToastrService, [{
      key: "toast",
      value: function toast(type, message) {
        toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": true,
          "progressBar": true,
          "positionClass": "toast-top-right",
          "preventDuplicates": true,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        };
        toastr[type](message);
      }
    }]);

    return ToastrService;
  }();

  return new ToastrService();
}];

/***/ }),

/***/ "./resources/assets/system/angular/system-controller.js":
/*!**************************************************************!*\
  !*** ./resources/assets/system/angular/system-controller.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ['$scope', 'LetterSize', function ($scope, LetterSize) {
  var letterSize = LetterSize.getInstance();

  $scope.increaseLetters = function () {
    letterSize.increaseSize();
  };

  $scope.decreaseLetters = function () {
    letterSize.decreaseSize();
  };
}];

/***/ }),

/***/ "./resources/assets/system/angular/turmas/turma-controller.js":
/*!********************************************************************!*\
  !*** ./resources/assets/system/angular/turmas/turma-controller.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ['$scope', '$window', function ($scope, $window) {
  $scope.turmas = _turmas;
}];

/***/ }),

/***/ "./resources/assets/system/angular/turmas/turma-create-controller.js":
/*!***************************************************************************!*\
  !*** ./resources/assets/system/angular/turmas/turma-create-controller.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ['$scope', '$window', 'TurmaService', function ($scope, $window, TurmaService) {
  $scope.turma = {};

  $scope.save = function () {
    TurmaService.store($scope.turma);
  };
}];

/***/ }),

/***/ "./resources/assets/system/angular/turmas/turma-service.js":
/*!*****************************************************************!*\
  !*** ./resources/assets/system/angular/turmas/turma-service.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports = ['$http', 'ToastrService', function ($http, ToastrService) {
  var TurmaService =
  /*#__PURE__*/
  function () {
    function TurmaService() {
      _classCallCheck(this, TurmaService);
    }

    _createClass(TurmaService, [{
      key: "store",
      value: function store(data) {
        $http.post('store', data).then(function (response) {
          if (response.status == 200 && !response.data.error) {
            ToastrService.toast('success', response.data.message);
          }
        }, function (error) {
          if (error.data.error) {
            ToastrService.toast('error', error.data.message.title[0]);
          }
        });
      }
    }]);

    return TurmaService;
  }();

  return new TurmaService();
}];

/***/ }),

/***/ "./resources/assets/system/angular/atividade/atividade-controller.js":
/*!********************************************************************!*\
  !*** ./resources/assets/system/angular/atividade/atividade-controller.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

    module.exports = ['$scope', '$window', function ($scope, $window) {
        $scope.atividade = _atividade;
      }];
      
      /***/ }),
      
      /***/ "./resources/assets/system/angular/atividade/atividade-create-controller.js":
      /*!***************************************************************************!*\
        !*** ./resources/assets/system/angular/atividade/atividade-create-controller.js ***!
        \***************************************************************************/
      /*! no static exports found */
      /***/ (function(module, exports) {
      
      module.exports = ['$scope', '$window', 'AtividadeService', function ($scope, $window, AtividadeService) {
        $scope.atividade = {};
      
        $scope.save = function () {
          AtividadeService.store($scope.atividade);
        };
      }];
      
      /***/ }),
      
      /***/ "./resources/assets/system/angular/atividade/atividade-service.js":
      /*!*****************************************************************!*\
        !*** ./resources/assets/system/angular/atividade/atividade-service.js ***!
        \*****************************************************************/
      /*! no static exports found */
      /***/ (function(module, exports) {
      
      function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
      
      function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }
      
      function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }
      
      module.exports = ['$http', 'ToastrService', function ($http, ToastrService) {
        var AtividadeService =
        /*#__PURE__*/
        function () {
          function AtividadeService() {
            _classCallCheck(this, AtividadeService);
          }
      
          _createClass(AtividadeService, [{
            key: "store",
            value: function store(data) {
              $http.post('store', data).then(function (response) {
                if (response.status == 200 && !response.data.error) {
                  ToastrService.toast('success', response.data.message);
                }
              }, function (error) {
                if (error.data.error) {
                  ToastrService.toast('error', error.data.message.title[0]);
                }
              });
            }
          }]);
      
          return AtividadeService;
        }();
      
        return new AtividadeService();
      }];
      
      /***/ }),

/***/ 0:
/*!*******************************************************!*\
  !*** multi ./resources/assets/system/angular/main.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Applications/MAMP/htdocs/ativadi/api/resources/assets/system/angular/main.js */"./resources/assets/system/angular/main.js");


/***/ })

},[[0,"/system/angular/manifest"]]]);