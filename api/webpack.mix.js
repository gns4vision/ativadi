const mix = require('laravel-mix');
mix.setPublicPath(path.normalize('../public'));
 
mix.copy('node_modules/toastr/toastr.min.css', 'public/css/toastr.css');
mix.copy('node_modules/toastr/toastr.min.js', 'public/js/toastr.js');
// mix.js('resources/assets/js/app.js', 'js')
// .sass('resources/assets/sass/app.scss', 'css');

mix.js('resources/assets/system/angular/main.js', 'system/angular')
.extract([
    'angular',
    'angular-input-masks',
    'toastr'
]);

mix.version();


