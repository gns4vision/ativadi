module.exports = [
    function(
    ) {
        class LetterSize {
            constructor() {
            }

            static getInstance() {
                if(this.instance == null || this.instance == undefined) {
                    this.instance = new LetterSize();
                }
                
                return this.instance;
            }

            increaseSize() {
                var p = document.getElementsByTagName("p");
                var h1 = document.getElementsByTagName("h1");
                var h2 = document.getElementsByTagName("h2");
                var h3 = document.getElementsByTagName("h3");
                var h4 = document.getElementsByTagName("h4");
                var h5 = document.getElementsByTagName("h5");
                var h6 = document.getElementsByTagName("h6");
                var strong = document.getElementsByTagName("strong");
                var span = document.getElementsByTagName("span");
                var th = document.getElementsByTagName("th");
                
                var td = document.getElementsByTagName("td");
                var i = document.getElementsByTagName("i");
                var a = document.getElementsByTagName("a");

                for(let incre=0; incre<p.length; incre++){
                    p[incre].style.fontSize = "large";
                }

                for(let incre=0; incre<h1.length; incre++){
                    h1[incre].style.fontSize = "large";
                }
                for(let incre=0; incre<h2.length; incre++){
                    h2[incre].style.fontSize = "large";
                }
                for(let incre=0; incre<h3.length; incre++){
                    h3[incre].style.fontSize = "large";
                }
                for(let incre=0; incre<h4.length; incre++){
                    h4[incre].style.fontSize = "large";
                }
                for(let incre=0; incre<h5.length; incre++){
                    h5[incre].style.fontSize = "large";
                }
                for(let incre=0; incre<h6.length; incre++){
                    h6[incre].style.fontSize = "large";
                }
                for(let incre=0; incre<strong.length; incre++){
                    strong[incre].style.fontSize = "large";
                }
                for(let incre=0; incre<span.length; incre++){
                    span[incre].style.fontSize = "large";
                }
                for(let incre=0; incre<th.length; incre++){
                    th[incre].style.fontSize = "large";
                }
                for(let incre=0; incre<td.length; incre++){
                    td[incre].style.fontSize = "large";
                }
                for(let incre=0; incre<i.length; incre++){
                    i[incre].style.fontSize = "large";
                }
                for(let incre=0; incre<a.length; incre++){
                    a[incre].style.fontSize = "large";
                }
            }
            
            decreaseSize() {
                var p = document.getElementsByTagName("p");
                var h1 = document.getElementsByTagName("h1");
                var h2 = document.getElementsByTagName("h2");
                var h3 = document.getElementsByTagName("h3");
                var h4 = document.getElementsByTagName("h4");
                var h5 = document.getElementsByTagName("h5");
                var h6 = document.getElementsByTagName("h6");
                var strong = document.getElementsByTagName("strong");
                var span = document.getElementsByTagName("span");
                var th = document.getElementsByTagName("th");
                
                var td = document.getElementsByTagName("td");
                var i = document.getElementsByTagName("i");
                var a = document.getElementsByTagName("a");

                for(let incre=0; incre<p.length; incre++){
                    p[incre].style.fontSize = "initial";
                }

                for(let incre=0; incre<h1.length; incre++){
                    h1[incre].style.fontSize = "initial";
                }
                for(let incre=0; incre<h2.length; incre++){
                    h2[incre].style.fontSize = "initial";
                }
                for(let incre=0; incre<h3.length; incre++){
                    h3[incre].style.fontSize = "initial";
                }
                for(let incre=0; incre<h4.length; incre++){
                    h4[incre].style.fontSize = "initial";
                }
                for(let incre=0; incre<h5.length; incre++){
                    h5[incre].style.fontSize = "initial";
                }
                for(let incre=0; incre<h6.length; incre++){
                    h6[incre].style.fontSize = "initial";
                }
                for(let incre=0; incre<strong.length; incre++){
                    strong[incre].style.fontSize = "initial";
                }
                for(let incre=0; incre<span.length; incre++){
                    span[incre].style.fontSize = "initial";
                }
                for(let incre=0; incre<th.length; incre++){
                    th[incre].style.fontSize = "initial";
                }
                for(let incre=0; incre<td.length; incre++){
                    td[incre].style.fontSize = "initial";
                }
                for(let incre=0; incre<i.length; incre++){
                    i[incre].style.fontSize = "initial";
                }
                for(let incre=0; incre<a.length; incre++){
                    a[incre].style.fontSize = "initial";
                }
            }
            
        }

        return LetterSize;
    }
];