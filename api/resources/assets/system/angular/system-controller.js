module.exports = [
    '$scope',
    'LetterSize',
    function (
        $scope,
        LetterSize
    ) {
        var letterSize = LetterSize.getInstance();
        
        $scope.increaseLetters = () => {
            letterSize.increaseSize();
        }

        $scope.decreaseLetters = () => {
            letterSize.decreaseSize();
        }
        
    }
];
