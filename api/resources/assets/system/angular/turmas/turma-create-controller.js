module.exports = [
    '$scope', 
    '$window', 
    'TurmaService',
    function (
        $scope, 
        $window,
        TurmaService
    ) {
        $scope.turma = {};

        $scope.save = () => {
            TurmaService.store($scope.turma);
        }
    }
];