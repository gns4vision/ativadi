module.exports = [
    '$http',
    function (
        $http
    ) {
        
        
        class AuditoriaService {
            constructor() {
            }

            store(data) {
                $http.post('store', data)
                .then(
                    (response) => {
                        console.log(response);
                    },
                    (error) => {
                        console.log(error);
                        
                    }
                )
            }
        }
        
        return new AuditoriaService();
    }
];