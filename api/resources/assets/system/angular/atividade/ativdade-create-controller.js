module.exports = [
    '$scope', 
    '$window', 
    'atividadeService',
    function (
        $scope, 
        $window,
        AtividadeService
    ) {
        $scope.atividade = {};

        $scope.save = () => {
            atividadeService.store($scope.atividade);
        }
    }
];