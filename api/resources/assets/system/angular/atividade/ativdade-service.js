module.exports = [
    '$http',
    'ToastrService',
    function (
        $http,
        ToastrService
    ) {
                
        class AtividadeService {
            constructor() {
            }

            store(data) {
                $http.post('store', data)
                .then(
                    (response) => {
                        if(response.status == 200 && !response.data.error) {
                            ToastrService.toast('success', response.data.message);
                        }
                    },
                    (error) => {
                        if(error.data.error) {
                            ToastrService.toast('error', error.data.message.title[0]);
                        }
                    }
                )
            }
        }
        
        return new AtividadeService();
    }
];