var app = angular.module('app', [
    'ui.utils.masks',
])

.controller('SystemController', require('./system-controller'))
.controller('TurmaController', require('./turmas/turma-controller'))
.controller('TurmaCreateController', require('./turmas/turma-create-controller'))
.controller('AuditoriaController', require('./auditorias/auditoria-controller'))
.controller('AtividadeController', require('./atividade/atividade-controller'))
.controller('AtividadeCreateController', require('./atividade/atividade-create-controller'))
.service('LetterSize', require('./lengthLetters'))
.service('TurmaService', require('./turmas/turma-service'))
.service('AtividadeService', require('./atividade/atividade-service'))
.service('ToastrService', require('./observer/toastrService'))

