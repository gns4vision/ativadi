<!DOCTYPE html>
<html lang="pt-br">
<head>

    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/css/skins/_all-skins.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('css/novais.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/toastr.css') }}" rel="stylesheet"/>

    @stack('head')

</head>

<body class="skin-blue" ng-app="app" ng-controller="SystemController">
    <div class="wrapper">

        @include('system.widgets.header')

        @include('system.widgets.sidebar')

        <div class="content-wrapper">
            @yield('content')
        </div>        

        @include('system.widgets.footer')

    </div>

    <!-- Angular -->
    <script src="{{ asset('system/angular/manifest.js') }}"></script>
    <script src="{{ asset('system/angular/vendor.js') }}"></script>
    <script src="{{ asset('system/angular/main.js') }}"></script>
    <!-- Angular fim -->

    <script src="{{ asset('plugins/jQuery/jQuery-2.1.3.min.js') }}"></script>

    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('dist/js/app.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/toastr.js') }}"></script>
    @stack('linkscripts')
    @stack('scripts')
</body>

</html>