<?php

class Dialogo
{
    //protected $mensagemDescricao;
    // Segura uma instância de uma classe'
    private static $instance;

    private function __construct() { }

    // O método Singleton
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new Dialogo();
            echo" Instância criada!";
        }
        return self::$instance;
    } 

    public function criarDialogo()
    {
        return "Alerta criado!";
    }        
}
$dialogo1 = Dialogo::getInstance();
$dialogo2 = Dialogo::getInstance();
$dialogo3 = Dialogo::getInstance();

$pt = new Dialogo();
$pt->criarDialogo();

/*$dialogo2->criarDialogo();*/    

/** Pesquisar **/
$pesq = false;

if(isset($_GET['pesquisar'])){

    echo"No pesquisar";
    $pesq = true;
}
?>


<form id="form1" action="" method="GET">
<div class="row padding-10">
    <div class="col-xs-12">
        <div class="box padding-10">
            <div class="box-header">
                <h1 class="box-grau">Aula - Singleton</h1>
            </div>                  
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">                
                        <tr>
                            <th>Data</th>
                            <th>Professor</th>
                            <th>Matéria</th>
                            <th>Turma</th>
                            <th>Atividade</th>                        
                        </tr>
                        <tr>
                            <td>12/09/2019 16:00</td>
                            <td>Vando</td>
                            <td>Português</td>
                            <td>ALFA 01</td>
                            <td>Alfabeto de letras</td>                        
                        </tr>
                        <tr>
                            <td> <?php print_r($dialogo1) ?> </td>
                            <td> <?php print_r($dialogo2) ?> </td>
                            <td> <?php print_r($dialogo3) ?> </td>
                        </tr>
                    </table>                                                                         
                </form>
                <div class="box-footer clearfix"">
                    <button class="btn btn-success" id="pesquisar" type="submit" value="" ><i class="fa fa-check"></i></button>
                    <input type="submit" name="pesquisar" id="pesquisar" value="" class="botao_pesquisar" />

                    <button class="btn btn-danger" id="CancelarAula"><i class="fa fa-times"></i></button>
                </div>                        
            </div>
        </div>
    </div>    
</div>
</form>