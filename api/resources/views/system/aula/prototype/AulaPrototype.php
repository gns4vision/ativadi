<?php
declare(strict_types=1);

namespace aula\prototype;

abstract class AulaPrototype
{
    /* @var string  */
    protected $materia;
    /* @var string */
    protected $atividade;

    abstract public function __clone();

    public function getMateria(): string
    {
        return $this->materia;
    }

    public function setTitle($materia)
    {
        $this->materia = $materia;
    }
}