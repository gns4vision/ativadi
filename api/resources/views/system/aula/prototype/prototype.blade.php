<?php
declare(strict_types=1);

namespace aula\prototype;

/*use PortuguesPrototype;*/
/*use MatematicaPrototype;*/
use PHPUnit\Framework\TestCase;

abstract class AulaPrototype
{
    /* @var string  */
    protected $data;    
    protected $professor;    
    protected $materia;
    protected $turma;    
    protected $atividade;

    abstract public function __clone();
    /* Data */
    public function getData(): string { return $this->data; }
    public function setData($data) { $this->data = $data; }

    /* Professor */
    public function getProfessor(): string { return $this->professor; }
    public function setProfessor($professor) { $this->professor = $professor; }    

    /* Materia */
    public function getMateria(): string { return $this->materia; }
    public function setMateria($materia) { $this->materia = $materia; }

    /* Turma */    
    public function getTurma(): string { return $this->turma; }
    public function setTurma($turma) { $this->turma = $turma; }    

    /* Atividade */    
    public function getAtividade(): string {return $this->atividade; }
    public function setAtividade($atividade) { $this->atividade = $atividade; }    
}

class PrototypeTest extends TestCase
{
    public $adescricao = array();

    public function testCanGetAula()
    {
        $PortuguesPrototype = new PortuguesPrototype();
        $MatematicaPrototype = new MatematicaPrototype();

        for ($i = 0; $i < 10; $i++) {
            $data = clone $PortuguesPrototype;
            $data->setData('Data: ' . $i);

            $professor = clone $PortuguesPrototype;
            $professor->setProfessor('João ' . $i);

            $materia = clone $PortuguesPrototype;
            $materia->setMateria('Português Nro ' . $i);

            $turma = clone $PortuguesPrototype;
            $turma->setTurma('ABC ' . $i);

            $atividade = clone $PortuguesPrototype;
            $atividade->setAtividade('Letras ' . $i);

            $this->assertInstanceOf(PortuguesPrototype::class, $data);
            $this->assertInstanceOf(PortuguesPrototype::class, $professor);
            $this->assertInstanceOf(PortuguesPrototype::class, $materia);
            $this->assertInstanceOf(PortuguesPrototype::class, $turma);
            $this->assertInstanceOf(PortuguesPrototype::class, $atividade);
            //echo "Português: ".$i."<br/>";            
            $this->adescricao[$i][0] = $data->getData();
            $this->adescricao[$i][1] = $professor->getProfessor();
            $this->adescricao[$i][2] = $materia->getMateria();
            $this->adescricao[$i][3] = $turma->getTurma();
            $this->adescricao[$i][4] = $atividade->getAtividade();

            /*array_push($this->adescricao, $data->getData());
            array_push($this->adescricao, $professor->getProfessor());
            array_push($this->adescricao, $materia->getMateria());
            array_push($this->adescricao, $turma->getTurma());
            array_push($this->adescricao, $atividade->getAtividade());*/

            //array_push($this->adescricao, $materia->getMateria(), $this->adescricao, $professor->getProfessor());
        }
        
        /* Ajustar a chamada para implementar utilizando clone  */
        for ($i = 0; $i < 5; $i++) {
            $materia = clone $MatematicaPrototype;
            $materia->setMateria('Materia Nro ' . $i);
            $this->assertInstanceOf(MatematicaPrototype::class, $materia);
            //echo"Matemática: ".$i."<br/>";
        }
    }
}

class PortuguesPrototype extends AulaPrototype
{
    /* @var string */
    protected $category = 'Portugues';

    public function __clone()
    {
    }
}

class MatematicaPrototype extends AulaPrototype
{
    /*
     * @var string
     */
    protected $category = 'Matematica';

    public function __clone()
    {
    }
}

$pt = new PrototypeTest();
$pt->testCanGetAula();

//echo $pt->adescricao;
//foreach(get_object_vars($pt) as $var => $value);
//var_dump($pt->adescricao);

?>

<div class="row padding-10">
    <div class="col-xs-12">
        <div class="box padding-10">
            <div class="box-header">
                <h1 class="box-grau">Aula - Prototype</h1>
            </div>                  
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">                
                        <tr>
                            <th>Data</th>
                            <th>Professor</th>
                            <th>Matéria</th>
                            <th>Turma</th>
                            <th>Atividade</th>                        
                        </tr>
                        <tr>
                            <td>12/09/2019 16:00</td>
                            <td>Vando</td>
                            <td>Português</td>
                            <td>ALFA 01</td>
                            <td>Alfabeto de letras</td>                        
                        </tr>
                        <?php 
                            $valor=1;
                            for ($i = 1; $i < 10; $i++) {
                                if($valor>0){
                                    $color="tr-gray";
                                    $valor=0;                                    
                                  }else{
                                    $color="";
                                    $valor=1;                                    
                                  }
                        ?>
                        <tr <?php echo "class='".$color."'" ?> >
                            <?php   
                                for ($j = 0; $j < 5; $j++) {
                            ?>
                                <td> <?php print_r($pt->adescricao[$i][$j]) ?> </td>     
                            <?php
                                }
                            ?>
                        </tr>
                        <?php
                            }
                        ?>
                    </table>                                                                         
                </form>
                <div class="box-footer clearfix"">
                    <button class="btn btn-success" id="ConfirmarAula"><i class="fa fa-check"></i></button>
                    <button class="btn btn-danger" id="CancelarAula"><i class="fa fa-times"></i></button>
                </div>                        
            </div>
        </div>
    </div>    
</div>