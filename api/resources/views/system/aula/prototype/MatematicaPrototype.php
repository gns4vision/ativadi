<?php
declare(strict_types=1);

namespace prototype;

class MatematicaPrototype extends AulaPrototype
{
    /*
     * @var string
     */
    protected $category = 'Matematica';

    public function __clone()
    {
    }
}