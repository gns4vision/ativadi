<?php
declare(strict_types=1);

namespace aula\prototype;

class PortuguesPrototype extends AulaPrototype
{
    /* @var string */
    protected $category = 'Portugues';

    public function __clone()
    {
    }
}