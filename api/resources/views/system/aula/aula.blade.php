<div class="row padding-10">
    <div class="col-xs-12">
        <div class="box padding-10">
            <div class="box-header">
                <h1 class="box-title">Todas as Aulas</h1>
                <div class="pull-right box-tools">
                    <button class="btn btn-success" a href="aula/editar" onclick="window.open('aula/editar', 'myWin', 'toolbar=no, directories=no, location=no, status=yes, menubar=no, resizable=no, scrollbars=yes, width=800, height=600'); return false" >Nova Aula</button>
                </div>
            </div>                        
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <!-- <tr>
                        <th><input type="text" class="form-control" placeholder="Data"></th>
                        <th><input type="text" class="form-control" placeholder="Professor"></th>
                        <th><input type="text" class="form-control" placeholder="Matéria"></th>
                        <th><input type="text" class="form-control" placeholder="Turma"></th>
                        <th><input type="text" class="form-control" placeholder="Atividade"></th>
                        <th><button class="btn btn-success" a href="materia/editar" onclick="window.open('aula/editar', 'myWin', 'toolbar=no, directories=no, location=no, status=yes, menubar=no, resizable=no, scrollbars=yes, width=800, height=600'); return false" >Incluir</button></th>
                        <th><button class="btn btn-warning">Editar</button></th>
                        <th><button class="btn btn-danger">Excluir</button></th>
                    </tr> -->                    
                    <tr>
                        <th>Data</th>
                        <th>Professor</th>
                        <th>Matéria</th>
                        <th>Turma</th>
                        <th>Atividade</th>                        
                    </tr>
                    <tr>
                        <td>12/09/2019 16:00</td>
                        <td>Vando</td>
                        <td>Português</td>
                        <td>ALFA 01</td>
                        <td>Alfabeto de letras</td>                        
                    </tr>
                    <tr class="tr-gray">
                        <td>12/09/2019 16:00</td>
                        <td>Simone</td>
                        <td>Matemática</td>
                        <td>BRAVO 01</td>
                        <td>Somatório</td>   
                    </tr>
                    <tr>
                        <td>12/09/2019 16:00</td>
                        <td>Gabriel</td>
                        <td>Geografia</td>
                        <td>CHARLIE 01</td>
                        <td>Estados do Brasil</td>   
                    </tr>
                    <tr class="tr-gray">
                        <td>12/09/2019 16:00</td>
                        <td>Caio</td>
                        <td>Ciências</td>
                        <td>DELTA 01</td>
                        <td>Sistema Solar</td>   
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>