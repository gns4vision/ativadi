<div class="row padding-10">
    <div class="col-xs-12">
        <div class="box padding-10">
            <div class="box-header">
                <h3 class="box-title">Matéria - Incluir/Editar</h3>
            </div>
            <div class="box-body">
                <form action="#" method="post" class="ng-pristine ng-valid">
                        <div class="form-group">
                            <input type="email" class="form-control" name="nome" placeholder="Nome:">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="grau" placeholder="Grau:">
                        </div>
                        <div>
                            <textarea class="textarea" placeholder="Objetivos" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="assunto" placeholder="Assunto:">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="Situacao" placeholder="Situação:">
                        </div>                                                        
                </form>
                <div class="box-footer clearfix"">
                    <button class="pull-left btn btn-success" id="ConfirmarMateria">Confirmar</button>
                    <button class="pull-left btn btn-danger" id="CancelarMateria">Cancelar</button>
                    <!--<button class="pull-right btn btn-default" id="sendEmail">Send <i class="fa fa-arrow-circle-right"></i></button>-->
                </div>                        
            </div>
        </div>
    </div>
</div>