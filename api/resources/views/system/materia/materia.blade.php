<div class="row padding-10">
    <div class="col-xs-12">
        <div class="box padding-10">
            <div class="box-header">
                <h3 class="box-title">Matéria!</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th><input type="text" class="form-control" placeholder="Nome"></th>
                        <th><input type="text" class="form-control" placeholder="Grau"></th>
                        <!-- <th><input type="text" class="form-control" placeholder="Objetivos"></th> -->
                        <th><textarea rows="4" cols="50" class="form-control" placeholder="Objetivos"> </textarea></th>
                        <th><input type="text" class="form-control" placeholder="Assunto"></th>
                        <th><input type="text" class="form-control" placeholder="Situação"></th>
                        <th><button class="btn btn-success" a href="materia/editar" onclick="window.open('materia/editar', 'myWin', 'toolbar=no, directories=no, location=no, status=yes, menubar=no, resizable=no, scrollbars=yes, width=800, height=600'); return false" >Incluir</button></th>
                        <th><button class="btn btn-warning">Editar</button></th>
                        <th><button class="btn btn-danger">Excluir</button></th>                        
                    </tr>
                    <tr>
                        <th>Nome</th>
                        <th>Grau</th>
                        <th>Objetivos</th>
                        <th>Assunto</th>
                        <th>Situação</th>                        
                    </tr>
                    <tr>
                        <td>Português</td>
                        <td>Básico</td>
                        <td>Aprender o Alfabeto</td>
                        <td> Alfabeto</td>
                        <td>Ativo</td>                        
                    </tr>
                    <tr class="tr-gray">
                        <td>Matemática</td>
                        <td>Básico</td>
                        <td>Aprender a somar</td>
                        <td>Somar de números positivos</td>
                        <td>Ativo</td>   
                    </tr>
                    <tr>
                        <td>Geografia</td>
                        <td>Básico</td>
                        <td>Aprender o nome dos estados brasileiros</td>
                        <td>Estados Brasileiros</td>
                        <td>Ativo</td>   
                    </tr>
                    <tr class="tr-gray">
                        <td>Ciências</td>
                        <td>Básico</td>
                        <td>Aprender os planetas do Sistema Solar</td>
                        <td>Sistema Solar</td>
                        <td>Ativo</td>   
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>