@extends('layouts.system')

@section('title')
AtivaDI
@endsection

@push('head')
@endpush

@section('content')
<div ng-controller="TurmaController">
    <section class="content-header">
        <h1>
            Turmas
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Turmas</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <section class="col-lg-12 connectedSortable">
                <div class="box">
                    <div class="box-header">
                        <a href="{{ url('turmas/add') }}" class="btn btn-success pull-right">Nova turma <i class="fa fa-plus" aria-hidden="true"></i></a>
                    </div>
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Título</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="turma in turmas">
                                    <td>@{{ turma.id }}</td>
                                    <td>@{{ turma.title }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </section>
</div>

@endsection

@push('linkscripts')
<script>
    var _turmas = {!! $turmas !!};
</script>
@endpush

@push('scripts')
@endpush