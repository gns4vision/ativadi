@extends('layouts.system')

@section('title')
AtivaDI
@endsection

@push('head')
@endpush

@section('content')
<div ng-controller="TurmaCreateController">
    <section class="content-header">
        <h1>
            Turmas
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Turmas</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <section class="col-lg-12 connectedSortable">
                <div class="box box-info">

                    <div class="box-body">
                        <form action="#" method="post">
                            @csrf
                            <div class="form-group">
                                <input type="text" class="form-control" ng-model="turma.title" placeholder="Título" />
                            </div>
                        </form>
                    </div>
                    <div class="box-footer clearfix">
                        <button class="pull-right btn btn-success" ng-click="save()">
                            Salvar <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>

            </section>

        </div>
    </section>
</div>

@endsection

@push('linkscripts')
@endpush

@push('scripts')
@endpush