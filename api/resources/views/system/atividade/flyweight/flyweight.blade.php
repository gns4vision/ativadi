<?php

interface FlyweightAtividade {
    /*private $nome;
    private $grau;
    private $link;*/

    /*function __construct($nome_in, $grau_in, $link_in) {
        $this->nome = $nome_in;
        $this->grau = $grau_in;
        $this->link = $link_in;        
    } */     

    /*
    function getNome() {
        return $this->nome;
    }    

    function getGrau() {
        return $this->grau;
    }

    function getLink() {
        return $this->link;
    } 
    */
    
    public function compartilharAtividade();
}

class FlyweightFactory {
    private $atividade = array(); 

    function __construct() {
        $this->atividade[1] = NULL;
        $this->atividade[2] = NULL;
        $this->atividade[3] = NULL;
    }
  
    function getAtividade($atividadeKey) {
        if (NULL == $this->atividade[$atividadeKey]) {
            $makeFunction = 'makeAtividade'.$atividadeKey;
            $this->atividade[$atividadeKey] = $this->$makeFunction(); 
        } 
        return $this->atividade[$atividadeKey];
    }    

    //Cada função abaixo poderá ficar em uma outra classe.
    function makeAtividade1() {
        $atividade = new FlyweightAtividade('Alfabeto','1','www.google.com.br'); 
        return $atividade;
    }
    function makeAtividade2() {
        $atividade = new FlyweightAtividade('Adjetivos','1','www.google.com.br'); 
        return $atividade;
    }
    function makeAtividade3() {
        $atividade = new FlyweightAtividade('Substantivos','3','www.uol.com.br'); 
        return $atividade;  
    }
}

class FlyweightAtividadeMontar extends FlyweightAtividade {
    private $atividades = array();

    public $adescricao;

    function addatividade($atividade) {
        $this->atividades[] = $atividade;
    }    

    function mostrarAtividades() {
        $return_string = NULL;

        $i = 0;
        foreach ($this->atividades as $atividade) {
            $return_string = "Nome: ".$atividade->getNome()." grau: ".$atividade->getGrau()." link: ".$atividade->getLink();
            //echo"Nome: ".$atividade->getNome()." grau: ".$atividade->getGrau()." link: ".$atividade->getLink();

            $this->adescricao[$i][0] = $atividade->getNome();
            $this->adescricao[$i][1] = $atividade->getGrau();
            $this->adescricao[$i][2] = $atividade->getLink();
            $i = $i + 1;
        };
        $this->descricao = $return_string;
        return $return_string;
    }
}

$flyweightFactory = new FlyweightFactory();
$FlyweightAtividadeMontar1 =  new FlyweightAtividadeMontar();

$flyweightAtividade1 = $flyweightFactory->getAtividade(1);
$FlyweightAtividadeMontar1->addAtividade($flyweightAtividade1);

$flyweightAtividade2 = $flyweightFactory->getAtividade(2);
$FlyweightAtividadeMontar1->addAtividade($flyweightAtividade2);

$flyweightAtividade3 = $flyweightFactory->getAtividade(3);
$FlyweightAtividadeMontar1->addAtividade($flyweightAtividade3);

//
$FlyweightAtividadeMontar1->mostrarAtividades();

?>

<div class="row padding-10">
    <div class="col-xs-12">
        <div class="box padding-10">
            <div class="box-header">
                <h1 class="box-grau">Atividades - Flyweight</h1>
            </div>                        
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">                
                    <tr>
                        <th>Nome</th>
                        <th>Grau</th>
                        <th>Link Acesso Externo</th>
                    </tr>
                    <tr>
                        <td>Alfabeto Teste</td>
                        <td>1</td>
                        <td>https://www.uol.com.br</td>
                    </tr>
                    <?php 
                            $valor=1;
                            for ($i = 0; $i < 3; $i++) {
                                if($valor>0){
                                    $color="tr-gray";
                                    $valor=0;                                    
                                  }else{
                                    $color="";
                                    $valor=1;                                    
                                  }
                        ?>
                        <tr <?php echo "class='".$color."'" ?> >
                            <?php   
                                for ($j = 0; $j < 3; $j++) {
                            ?>
                                <td> <?php print_r($FlyweightAtividadeMontar1->adescricao[$i][$j]) ?> </td>     
                            <?php
                                }
                            ?>
                        </tr>
                        <?php
                            }
                        ?>
                    <tr>                    
                    </tr>                    
                </table>
            </div>
        </div>
    </div>
</div>