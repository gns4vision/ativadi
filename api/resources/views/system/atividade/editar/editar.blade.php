<div class="row padding-10">
    <div class="col-xs-12">
        <div class="box padding-10">
            <div class="box-header">
                <h3 class="box-title">Atividade - Incluir/Editar</h3>
            </div>
            <div class="box-body">
                <form action="#" method="post" class="ng-pristine ng-valid">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" name="nome" placeholder="Nome:">
                                <div class="input-group-btn">
                                    <button class="btn btn-light"><i class="fa fa-search"></i></button>
                                </div>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                    <input typ  e="text" class="form-control" name="grau" placeholder="Grau:">
                                    <div class="input-group-btn">
                                        <button class="btn btn-light"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" name="link" placeholder="Link Acesso Externo:">
                                <div class="input-group-btn">
                                    <button class="btn btn-light"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>                                                                                                                                  
                </form>
                <div class="box-footer clearfix"">
                    <button class="btn btn-success" id="ConfirmarAtividade"><i class="fa fa-check"></i></button>
                    <button class="btn btn-danger" id="ConfirmarAtividade"><i class="fa fa-times"></i></button>
                </div>                        
            </div>
        </div>
    </div>
</div>