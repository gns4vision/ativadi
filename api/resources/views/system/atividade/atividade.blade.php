<div class="row padding-10">
    <div class="col-xs-12">
        <div class="box padding-10">
            <div class="box-header">
                <h1 class="box-title">Todas as Atividades</h1>
                <div class="pull-right box-tools">
                    <button class="btn btn-success" a href="atividade/editar" onclick="window.open('atividade/editar', 'myWin', 'toolbar=no, directories=no, location=no, status=yes, menubar=no, resizable=no, scrollbars=yes, width=800, height=600'); return false" >Nova Atividade</button>
                </div>                
            </div>                        
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">                
                    <tr>
                        <th>Nome</th>
                        <th>Grau</th>
                        <th>Link Acesso Externo</th>
                    </tr>
                    <tr>
                        <td>Alfabeto</td>
                        <td>1</td>
                        <td>https://www.google.com.br</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>