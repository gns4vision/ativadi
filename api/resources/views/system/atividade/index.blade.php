@extends('layouts.system')

@section('title')
AtivaDI
@endsection

@push('head')
@endpush

@section('content')
<div ng-controller="AtividadeController">
    <section class="content-header">
        <h1>
            Atividades
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">Atividades</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <section class="col-lg-12 connectedSortable">
                <div class="box">
                    <div class="box-header">
                        <a href="{{ url('atividade/create') }}" class="btn btn-success pull-right">Nova atividade <i class="fa fa-plus" aria-hidden="true"></i></a>
                    </div>
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Descrição</th>
                                    <th>Grau</th>
                                    <th>URL Externa</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="atividade in atividade">
                                    <td>@{{ atividade.id }}</td>
                                    <td>@{{ atividade.descricao }}</td>
                                    <td>@{{ atividade.grau }}</td>
                                    <td>@{{ atividade.url_externa }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </section>
</div>

@endsection

@push('linkscripts')
<script>
    /* Retorno do banco de dados */
    var _atividade = {!! $atividade !!};
</script>
@endpush

@push('scripts')
@endpush