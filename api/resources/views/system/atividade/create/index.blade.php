@extends('layouts.system')

@section('title')
AtivaDI
@endsection

@push('head')
@endpush

@section('content')
<div ng-controller="AtividadeCreateController">
    <section class="content-header">
        <h1>
            Atividades
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Atividades</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <section class="col-lg-12 connectedSortable">
                <div class="box box-info">

                    <div class="box-body">
                        <form action="#" method="post">
                            <!-- Token para requisição do formulários -->
                            @csrf
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="descricao" ng-model="atividade.descricao" placeholder="Descrição:">
                                    <!--<div class="input-group-btn">
                                        <button class="btn btn-light"><i class="fa fa-search"></i></button>
                                    </div> -->
                                </div>                            
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                        <input type="text" class="form-control" name="grau" ng-model="atividade.grau" placeholder="Grau:">
                                        <!--<div class="input-group-btn">
                                            <button class="btn btn-light"><i class="fa fa-search"></i></button>
                                        </div> -->
                                    </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="link" ng-model="atividade.url_externa" placeholder="Link Acesso Externo:">
                                    <!-- <div class="input-group-btn"> -->
                                        <!--  Para os botões de localizar baixar o ui-select do angular e executar o comando:#
                                        $ npm install ui-select --save 
                                        guarda no projeto para baixar em comum em todos os branches -->
                                    <!--    <button class="btn btn-light"><i class="fa fa-search"></i></button>
                                    </div> -->
                                </div>
                            </div>   
                        </form>
                    </div>
                    <div class="box-footer clearfix">
                        <button class="pull-right btn btn-success" ng-click="save()">
                            Salvar <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>

            </section>

        </div>
    </section>
</div>

@endsection

@push('linkscripts')
@endpush

@push('scripts')
@endpush