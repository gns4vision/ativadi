@extends('layouts.system')

@section('title')
AtivaDI - Auditoria
@endsection

@push('head')
@endpush

@section('content')

@include('system.auditoria.auditorias')

@endsection

@push('linkscripts')
<script>
    var _auditorias = {!! $auditorias !!};
</script>
@endpush

@push('scripts')
@endpush