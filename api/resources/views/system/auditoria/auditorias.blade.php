<div class="row padding-10" ng-controller="AuditoriaController">
    <div class="col-xs-12">
        <div class="box padding-10">
            <div class="box-header">
                <h3 class="box-title">Auditoria</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th><input type="text" class="form-control" placeholder="Buscar usuário"></th>
                        <th><input type="text" class="form-control" placeholder="Buscar data"></th>
                        <th><input type="text" class="form-control" placeholder="Buscar ação"></th>
                    </tr>
                    <tr class="tr-gray">
                        <th>Usuário</th>
                        <th>Data</th>
                        <th>Ação</th>
                    </tr>
                    <tr ng-class="auditoria.id%2 != 0 ? 'tr-gray' : ''" ng-repeat="auditoria in auditorias">
                        <td>@{{ auditoria.user.name }}</td>
                        <td>@{{ auditoria.created_at }}</td>
                        <td>@{{ auditoria.action }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>