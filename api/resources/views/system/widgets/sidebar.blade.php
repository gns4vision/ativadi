<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header">Menu</li>
            <li>
                <a href="{{ url('/') }}">
                    <i class="fa fa-bar-chart"></i> <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="{{ url('alunos') }}">
                    <i class="fa fa-users"></i> <span>Alunos</span>
                </a>
            </li>
            <li>
                <a href="{{ url('turmas') }}">
                    <i class="fa fa-id-card"></i> <span>Turmas</span>
                </a>
            </li>
            <li>
                <a href="{{ url('atividade') }}">
                    <i class="fa fa-id-card"></i> <span>Atividades</span>
                </a>
            </li>            
            <li>
                <a href="{{ url('auditoria') }}">
                    <i class="fa fa-pencil"></i> <span>Auditoria</span>
                </a>
            </li>
            <li>
                <a href="{{ url('turmas') }}">
                    <i class="fa fa-info-circle"></i> <span>Sobre</span>
                </a>
            </li>

        </ul>
    </section>
</aside>