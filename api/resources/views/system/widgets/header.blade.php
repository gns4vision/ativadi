<header class="main-header">
    <a href="{{ url('/') }}" class="logo">AtivaDI logo</a>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ asset('images/logo-utf.jpeg') }}" class="user-image" alt="User Image" />
                        <span class="hidden-xs">{{ Auth::user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="{{ asset('images/logo-utf.jpeg') }}" class="img-circle" alt="User Image" />
                            <p>
                                {{ Auth::user()->name }} - Web Developer
                                <small>Member since Nov. 2012</small>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Perfil</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('logout') }}" class="btn btn-default btn-flat"
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="fa fa-fw fa-power-off"></i>
                                    <span> Sair </span>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                        style="display: none;">{{
                            csrf_field() }}</form>
                                </a>
                                <!-- <a href="{{ url('logout') }}" class="btn btn-default btn-flat">Sair</a> -->
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
            <button class="btn btn-success btn-sm m-t-10" ng-click="increaseLetters()">A +</button>
            <button class="btn btn-warning btn-sm m-t-10" ng-click="decreaseLetters()">a -</button>
        </div>
    </nav>
</header>