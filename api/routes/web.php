<?php

Route::group(['middleware' => 'auth'], function(){

    Route::group(['namespace' => 'Dashboard'], function() {
        Route::get('/', 'DashboardController@index')->name('dashboard');
    });
    Route::group(['prefix' => 'turmas' ,'namespace' => 'Turmas'], function() {
        Route::get('/', 'TurmaController@index')->name('turma');
        Route::get('/add', 'TurmaController@create')->name('turma-create');
        Route::post('/store', 'TurmaController@store')->name('turma-store');
        
    });
    Route::group(['namespace' => 'Auditory'], function() {

        Route::get('/auditoria', 'AuditoryController@index')->name('auditory');
        
    });

    Route::group(['prefix' => 'materia', 'namespace' => 'Materia'], function() {

        Route::get('/', 'MateriaController@index')->name('materia');
        Route::get('/editar', 'MateriaController@editar')->name('editar');
        //Route::get('/materia/{id}/editar', 'MateriaController@editar')->name('editar');   
    });

    Route::group(['prefix' => 'aula', 'namespace' => 'Aula'], function() {

        Route::get('/', 'AulaController@index')->name('aula');
        Route::get('/editar', 'AulaController@editar')->name('editar');    
        Route::get('/prototype', 'AulaController@prototype')->name('prototype');        
        Route::get('/strategy', 'AulaController@strategy')->name('strategy');
        Route::get('/singleton', 'AulaController@singleton')->name('singleton');    
        
    });

    Route::group(['prefix' => 'atividade','namespace' => 'Atividade'], function() {

        Route::get('/', 'AtividadeController@index')->name('atividade');
        Route::get('/create', 'AtividadeController@create')->name('create');        
        Route::post('/store', 'AtividadeController@store')->name('atividade-store');
        Route::get('/editar', 'AtividadeController@editar')->name('editar');    
        Route::get('/flyweight', 'AtividadeController@flyweight')->name('flyweight');        
        
    });
});

Auth::routes();

Route::get('/home', 'HomeController@')->name('home');
