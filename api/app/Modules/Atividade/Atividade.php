<?php

namespace App\Modules\Atividade;

use Illuminate\Database\Eloquent\Model;

class Atividade extends Model
{
    protected $fillable = [
        'descricao',
        'grau',
        'url_externa',
    ];
    
}
