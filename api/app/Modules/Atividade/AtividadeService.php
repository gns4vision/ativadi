<?php

namespace App\Modules\Atividade;

use App\Modules\Auditing\AuditingService;
use App\Modules\Auditing\Strategy\StrategyAuditoria;
use Illuminate\Support\Facades\DB;

class AtividadeService
{

    public function __construct(Atividade $model, StrategyAuditoria $strategy_auditoria)
    {
        $this->model = $model;
        $this->strategy_auditoria = $strategy_auditoria;
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    public function get($id = null)
    {

        if ($id != null) {
            return $this->model->where('id', $id);
        }

        return $this->model;
    }

    public function store(array $data)
    {
        try {
            DB::beginTransaction();
            
            $model = $this->model->create($data);
            /* $this->strategy_auditoria->auditar(__METHOD__, $model); */

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        return $model;
    }

    public function update(array $data, int $id)
    {
        try {

            DB::beginTransaction();
            
            DB::commit();

        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }

        return null;
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }

        return true;
    }

    public function restore($id)
    {
        try {
            DB::beginTransaction();

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }

        return true;
    }

}