<?php

namespace App\Modules\Atividade;

use App\Modules\RootRequest;

class AtividadeRequest extends RootRequest
{
   
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'descricao' => 'required',
            'grau' => 'required',
            'url_externa' => 'required',            
        ];
    }

    public function attributeNames()
    {
        return [
            'descricao' =>  'Descrição',
            'grau' =>  'Grau',
            'url_externa' =>  'URL Externa',
        ];
    }

    public function messages()
{
    return [
        'descricao.required' => 'Informe o descrição.',
        'grau.required' => 'Informe o grau.',
        'url_externa.required' => 'Informe a URL externa.',
    ];
}
}
