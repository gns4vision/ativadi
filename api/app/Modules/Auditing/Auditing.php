<?php

namespace App\Modules\Auditing;

use Illuminate\Database\Eloquent\Model;

class Auditing extends Model
{
    protected $table = 'auditing';
    
    protected $guarded = [];
    
    protected $fillable = [
        'action', 'user_id'
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
