<?php

namespace App\Modules\Auditing\Strategy;

class StrategyAuditoria
{
    public function __construct(AuditoriaInterna $interna)
    {
        $this->interna = $interna;
    }
    
    public function auditar($model, $request) 
    {
        $this->interna->auditar($model, $request);
    }
}