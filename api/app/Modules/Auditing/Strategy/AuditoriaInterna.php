<?php

namespace App\Modules\Auditing\Strategy;

use App\Modules\Auditing\AuditingService;

class AuditoriaInterna
{
    public function __construct(AuditingService $auditing_service)
    {
        $this->auditing_service = $auditing_service;
    }
    
    public function auditar($model, $request) 
    {
        $this->auditing_service->store($model, $request);
    }
}