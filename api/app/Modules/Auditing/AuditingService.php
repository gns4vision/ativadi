<?php

namespace App\Modules\Auditing;

use App\Modules\Auditing\Auditing as AuditingModel;
use Illuminate\Support\Facades\Auth;

class AuditingService
{
    public function __construct(AuditingModel $model)
    {
        $this->model = $model;
        $this->relations = ['user'];
    }
    
    public function store($model, $request) 
    {
        $item = $request->toArray();
        $data = $this->prepareAuditing($model, $request);
        $action = '';

        if (!empty($data)) {
            if(strcmp($data['action'], 'store') == 0) {
                $action = "cadastrado";
            } else if(strcmp($data['action'], 'update') == 0) {
                $action = "editado";
            } else if(strcmp($data['action'], 'destroy') == 0) {
                $action = "deletado";
            }
        }

        $message_action = $data['model'] . ' - ' . array_values($item)[0] . ' - ' . $action .' com sucesso.';
        $data['action'] = $message_action;
        $data['user_id'] = Auth::user()->id;
        $model = $this->model->create($data);
    }

    public function prepareAuditing($model, $request)
    {
        $model_arr = explode('\\', $model);
        $action_arr = explode('::', $model_arr[3]) ;
        $data['model'] = $model_arr[2];   
        $data['action'] = $action_arr[1];  
        $data['item'] = $request; 
        return $data;
    }
}