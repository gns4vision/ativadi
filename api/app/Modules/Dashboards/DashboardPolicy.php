<?php

namespace App\Modules\Dashboards;

use Illuminate\Foundation\Auth\User;

class DashboardPolicy
{
    public function view(User $user)
    {
        return $this->can($user, 'view', 'dashboards');
    }

    public function create(User $user)
    {
        return $this->can($user, 'create', 'dashboards');
    }

    public function update(User $user)
    {
        return $this->can($user, 'update', 'dashboards');
    }

    public function delete(User $user)
    {
        return $this->can($user, 'delete', 'dashboards');
    }

    public function restore(User $user)
    {
        return $this->can($user, 'restore', 'dashboards');
    }
}
