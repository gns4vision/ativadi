<?php

namespace App\Modules\Dashboards;


class DashboardRequest
{
    public function authorize()
    {
        return !empty($this->user());
    }

    public function rules()
    {
        $id = $this->segment(3);

        if (empty($id)) {
            $rules = [
               
            ];
        } 

        return $rules;

    }

    public function attributeNames()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [
        ];
    }

}
