<?php

namespace App\Modules\Depoiments;

use FirstClass\Traits\Eventful;
use FirstClass\Traits\HasNgTable;

class DepoimentService
{
    use HasNgTable;
    use Eventful;

    public function __construct(Dashboard $model)
    {
        $this->model = $model;
    }
    public function get() {
        return [1, 2, 3, 4];
    }

    public function store(array $data)
    {
        return true;
    }

    public function update(array $data, int $id)
    {
        return true;
    }

    public function destroy($id)
    {
        return true;
    }

    public function restore($id)
    {
        return true;
    }

}