<?php

namespace App\Modules\Turmas;

use App\Modules\RootRequest;

class TurmaRequest extends RootRequest
{
   
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'title' => 'required'
        ];
    }

    public function attributeNames()
    {
        return [
            'title' =>  'Título',
        ];
    }

    public function messages()
{
    return [
        'title.required' => 'Informe o título.',
    ];
}
}
