<?php

namespace App\Modules\Turmas;

use Illuminate\Database\Eloquent\Model;

class Turma extends Model
{
    protected $fillable = [
        'title',
    ];
    
}
