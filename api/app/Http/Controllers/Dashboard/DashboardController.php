<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\Modules\Turmas\TurmaService;
use App\Modules\Atividade\AtividadeService;

class DashboardController extends Controller
{
    public function __construct(TurmaService $turma_service, AtividadeService $atividade_service) {
        $this->turma_service = $turma_service;
        $this->atividade_service = $atividade_service;
    }
   
    public function index()
    {
        $turmas = $this->turma_service->model->get();
        $atividade = $this->atividade_service->model->get();
        return view('system.dashboard.index');
    }
}
