<?php

namespace App\Http\Controllers\Atividade;
use App\Http\Controllers\Controller;
use App\Modules\Atividade\AtividadeRequest;
use App\Modules\Atividade\AtividadeService;

class AtividadeController extends Controller
{
    public function __construct(AtividadeService $atividade_service) {
        $this->atividade_service = $atividade_service;
    }

    public function index()
    {
        $atividade = $this->atividade_service->model->get();
        return view('system.atividade.index', compact('atividade'));
    }    

    public function create()
    {
        return view('system.atividade.create.index');
    }       

    public function editar()
    {
        return view('system.atividade.editar.index');
    }    

    public function flyweight()
    {
        return view('system.atividade.flyweight.index');
    }          

    public function store(AtividadeRequest $request)
    {
        $this->atividade_service->store($request->toArray());

        return response()->json([
            'error' => false,
            'message' => "Cadastrado com sucesso",
        ], 200);
    }
}
