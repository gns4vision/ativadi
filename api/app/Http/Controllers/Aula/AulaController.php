<?php

namespace App\Http\Controllers\Aula;
use App\Http\Controllers\Controller;

class AulaController extends Controller
{
   
    public function index()
    {
        return view('system.aula.index');
    }

    public function editar()
    {
        return view('system.aula.editar.index');
    }    

    public function prototype()
    {
        return view('system.aula.prototype.index');
    }

    public function strategy()
    {        
        return view('system.aula.strategy.index');
    }

    public function singleton()
    {        
        return view('system.aula.singleton.index');
    }    
}
