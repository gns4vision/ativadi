<?php

namespace App\Http\Controllers\Materia;
use App\Http\Controllers\Controller;

class MateriaController extends Controller
{
   
    public function index()
    {
        return view('system.materia.index');
    }

    public function editar()
    {
        return view('system.materia.editar.index');
    }    
}
