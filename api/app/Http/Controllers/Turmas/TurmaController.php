<?php

namespace App\Http\Controllers\Turmas;
use App\Http\Controllers\Controller;
use App\Modules\Turmas\TurmaRequest;
use App\Modules\Turmas\TurmaService;

class TurmaController extends Controller
{
    public function __construct(TurmaService $turma_service) {
        $this->turma_service = $turma_service;
    }
   
    public function index()
    {
        $turmas = $this->turma_service->model->get();
        return view('system.turmas.index', compact('turmas'));
    }

    public function create()
    {
        return view('system.turmas.create.index');
    }

    public function store(TurmaRequest $request)
    {
        $this->turma_service->store($request->toArray());

        return response()->json([
            'error' => false,
            'message' => "Cadastrado com sucesso",
        ], 200);
    }
}
