<?php

namespace App\Http\Controllers\Auditory;
use App\Http\Controllers\Controller;
use App\Modules\Auditing\AuditingService;

class AuditoryController extends Controller
{
   
    public function __construct(AuditingService $auditing_service)
    {
        $this->auditing_service = $auditing_service;
    }
    public function index()
    {
        $auditorias = $this->auditing_service->model->with('user')->get();
        return view('system.auditoria.index', compact('auditorias'));
    }
}
